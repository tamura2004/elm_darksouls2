import Color (..)
import Graphics.Element (..)
import Graphics.Input.Field (..)
import Graphics.Input as Input
import Signal
import Text
import Window
import String
import Maybe
import Array

-- MODEL

type Class
  = Warrior   -- 戦士
  | Knight    -- 騎士
  | Swordsman -- 剣士
  | Bandit    -- 野盗
  | Cleric    -- 聖職者
  | Sorcerer  -- 魔術師
  | Explorer  -- 探索者
  | Deprived  -- 持たざるもの

type Ability
  = SoulLevel    -- ソウルレベル
  | Vigor        -- 生命
  | Endurance    -- 持久
  | Vitality     -- 体力
  | Attunement   -- 記憶
  | Strength     -- 筋力
  | Dexterity    -- 敏捷
  | Adaptability -- 適応
  | Intelligence -- 理力
  | Faith        -- 信仰

type alias Model =
  { class        : Class
  , soulLevel    : Content
  , vigor        : Content
  , endurance    : Content
  , vitality     : Content
  , attunement   : Content
  , strength     : Content
  , dexterity    : Content
  , adaptability : Content
  , intelligence : Content
  , faith        : Content
  }

emptyModel =
  { class        = Warrior
  , soulLevel    = toContent 12
  , vigor        = toContent 7
  , endurance    = toContent 6
  , vitality     = toContent 6
  , attunement   = toContent 5
  , strength     = toContent 15
  , dexterity    = toContent 11
  , adaptability = toContent 5
  , intelligence = toContent 5
  , faith        = toContent 5
  }

-- DATA

a' = Array.fromList

classTable : Class -> Array.Array Int
classTable class =
    case class of
        Warrior     -> a' [12,7,6,6,5,15,11,5,5,5]
        Knight      -> a' [13,12,6,7,4,11,8,9,3,6]
        Swordsman   -> a' [12,4,8,4,6,9,16,6,7,5]
        Bandit      -> a' [11,9,7,11,2,9,14,3,1,8]
        Cleric      -> a' [14,10,3,8,10,11,5,4,4,12]
        Sorcerer    -> a' [11,5,6,5,12,3,7,8,14,4]
        Explorer    -> a' [10,7,6,9,7,6,6,12,5,5]
        Deprived    -> a' [1 ,6,6,6,6,6,6,6,6,6]

abilityIndex : Ability -> Int
abilityIndex ability =
    case ability of
        SoulLevel    -> 0
        Vigor        -> 1
        Endurance    -> 2
        Vitality     -> 3
        Attunement   -> 4
        Strength     -> 5
        Dexterity    -> 6
        Adaptability -> 7
        Intelligence -> 8
        Faith        -> 9

get : Class -> Ability -> Int
get class ability =
    Maybe.withDefault 0
        <| Array.get (abilityIndex ability) (classTable class)

newModel : Class -> Array.Array Int -> Model
newModel cls ary =
  { emptyModel |
    class        <- cls
  , soulLevel    <- toContent <| get cls SoulLevel
  , vigor        <- toContent <| get cls Vigor
  , endurance    <- toContent <| get cls Endurance
  , vitality     <- toContent <| get cls Vitality
  , attunement   <- toContent <| get cls Attunement
  , strength     <- toContent <| get cls Strength
  , dexterity    <- toContent <| get cls Dexterity
  , adaptability <- toContent <| get cls Adaptability
  , intelligence <- toContent <| get cls Intelligence
  , faith        <- toContent <| get cls Faith
  }

-- UPDATE

type Update
  = ChangeAbility Ability Content
  | SelectClass Class

update : Update -> Model -> Model
update updt model =
  case updt of

    ChangeAbility ability content ->
      case ability of
        SoulLevel     -> { model | soulLevel    <- content}

        Vigor         -> { model |
          vigor        <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.vigor))

        }

        Endurance     -> { model |
          endurance    <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.endurance))

        }
        Vitality      -> { model |
          vitality     <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.vitality))
        }
        Attunement    -> { model |
          attunement   <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.attunement))
        }
        Strength      -> { model |
          strength     <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.strength))
        }
        Dexterity     -> { model |
          dexterity    <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.dexterity))
        }
        Adaptability  -> { model |
          adaptability <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.adaptability))
        }
        Intelligence  -> { model |
          intelligence <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.intelligence))
        }
        Faith         -> { model |
          faith        <- content,
          soulLevel    <- toContent ((fromContent model.soulLevel) + (fromContent content) - (fromContent model.faith))
        }

    SelectClass cls ->
      newModel cls
        <| classTable cls

toContent : Int -> Content
toContent i =
  { noContent | string <- toString i}

fromContent : Content -> Int
fromContent content =
  let
    result = String.toInt content.string
  in
    case result of
      Ok num -> num
      Err msg -> 0

-- VIEW

view : (Int,Int) -> Model -> Element
view (w,h) model =
  color charcoal <|
    flow down
    [ spacer w 50
    , container w (h-50) midTop (viewForm model)
    ]

viewForm : Model -> Element
viewForm model =
  color lightGray <|
    flow down
    [ container 360 60 middle header
    , flow right
      [ viewDropdown
      , viewField "レベル" model.soulLevel (ChangeAbility SoulLevel)
      ]
    , (container 360 4 middle <| color charcoal <| spacer 340 2)
    , spacer 360 6
    , flow right
      [ flow down
        [ viewField "生命" model.vigor (ChangeAbility Vigor)
        , viewField "持久" model.endurance (ChangeAbility Endurance)
        , viewField "体力" model.vitality (ChangeAbility Vitality)
        , viewField "記憶" model.attunement (ChangeAbility Attunement)
        ]
      , flow down
        [ viewField "筋力" model.strength (ChangeAbility Strength)
        , viewField "技量" model.dexterity (ChangeAbility Dexterity)
        , viewField "適応" model.adaptability (ChangeAbility Adaptability)
        , viewField "理力" model.intelligence (ChangeAbility Intelligence)
        , viewField "信仰" model.faith (ChangeAbility Faith)
        ]
      ]
    , spacer 10 10
    ]

header : Element
header =
  Text.fromString "Dark Souls 2"
    |> Text.height 32
    |> Text.leftAligned


viewDropdown : Element
viewDropdown =
  container 150 48 midRight <|
    height 36 <|
      Input.dropDown (Signal.send updateChan << SelectClass)
        [ ("戦士", Warrior)
        , ("騎士", Knight)
        , ("剣士", Swordsman)
        , ("野盗", Bandit)
        , ("聖職者", Cleric)
        , ("魔術師", Sorcerer)
        , ("探索者", Explorer)
        , ("持たざるもの", Deprived)
        ]

viewField : String -> Content -> (Content -> Update) -> Element
viewField label content toUpdate =
  flow right
    [ container 72 48 midRight (Text.plainText label)
    , container 78 48 middle <|
        size 54 36 <|
          field myStyle (Signal.send updateChan << toUpdate) "00" content
    ]

myStyle : Style
myStyle =
  let
    t = Text.defaultStyle
    myTextStyle = { t | height <- Just 24 }
  in
    { defaultStyle | style <- myTextStyle }


-- SIGNALS

main : Signal Element
main = Signal.map2 view Window.dimensions model

model : Signal Model
model =
  Signal.subscribe updateChan
    |> Signal.foldp update emptyModel

updateChan : Signal.Channel Update
updateChan =
  Signal.channel (SelectClass Warrior)
